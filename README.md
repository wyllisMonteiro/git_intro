# Utilisation gitlab

## Légendes
- [] : pas obligatoire
- <> : à remplacer

## Cloner un projet
```sh
$ git clone <TON_URL>
```

## Importer un projet
```sh
$ cd <TON_DOSSIER>
$ git init
$ git remote add origin <CHEMIN_VERS_GIT_CLONE>
$ git add .
$ git commit -m "Initial commit"
$ git push -u origin master
```

## Commit
- Tracker ou suivre tes fichiers
```sh
$ git add . 
```
- Commit
```sh
$ git commit [-m "ton message"]
```

## Push
```sh
$ git push origin <TA_BRANCHE> 
```

## Pull
```sh
$ git pull origin <TA_BRANCHE>
```

## Autres

- Créer et changer de branche
```sh
$ git checkout -b <TA_NOUVELLE_BRANCHE>
```

- Voir le statut de ses fichiers pour commit ou non
```sh
$ git status
```

- Voir l'historique de commits
```sh
$ git log --oneline
```

- Voir les differences avec le dernier commit en local
```sh
$ git diff
```

- Stocker des changements qui ne sont pas commit
```sh
$ git stash
```

- Voir le repo distant "remote"
```sh
$ git remote -v
```

- Voir toutes les branche en local
```sh
$ git branch -v
```